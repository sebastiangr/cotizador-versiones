$(document).ready(function() {

    // Selector de horario (elemento-hora)
    var horaBlock = document.getElementsByClassName('elemento-hora');
    for (var i = 0; i < horaBlock.length; i++) {
      horaBlock[i].addEventListener("click", function(){

        var selectedBlock = document.querySelector(".selected");
        
        if(selectedBlock){
          selectedBlock.classList.remove("selected");
        }

        $('#next-step0').prop('disabled', false); // Activar botón siguiente en paso 1 al seleccionar cualquier item
        this.classList.add("selected");
        currencyCheck();
      }, false);;
    }



    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');

        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

    $('ul.setup-panel li.active a').trigger('click');

    // STEP 0 //
    $('.activate-step-0').on('click', function(e) {
      $('ul.setup-panel li:eq(0)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-0"]').trigger('click');
      // $('.menu-lateral-step1').show();
      // $('.menu-lateral-step2').hide();
      // $('.menu-lateral-step3').hide();
    });
    // STEP 1 //
    $('.activate-step-1').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-1"]').trigger('click');
        $('.menu-lateral-step1').show();
        $('.menu-lateral-step2').hide();
        $('.menu-lateral-step3').hide();
    });
    // STEP 2 //
    $('.activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $('.menu-lateral-step1').hide();
        $('.menu-lateral-step2').show();
        $('.menu-lateral-step3').hide();
        //$(this).remove();
    });
    // STEP 3 //
    $('.activate-step-3').on('click', function(e) {
      $('ul.setup-panel li:eq(3)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
      $('.menu-lateral-step1').hide();
      $('.menu-lateral-step2').hide();
      $('.menu-lateral-step3').show();
    });
    // STEP 4 //
    $('.activate-step-4').on('click', function(e) {
      $('ul.setup-panel li:eq(4)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-4"]').trigger('click');
      // $('.menu-lateral-step1').hide();
      // $('.menu-lateral-step2').hide();
      // $('.menu-lateral-step3').show();
    });   

    // BTN COTIZAR - IGUAL A SIGUIENTE EN STEP 3
    $('#btn-cotizar').on('click', function(e) {
      $('ul.setup-panel li:eq(2)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
      $('.menu-lateral-step1').hide();
      $('.menu-lateral-step2').hide();
      $('.menu-lateral-step3').show();
    });        


    // FORM VALIDATION - FALTAN
    // Validación campo Nombre (#nombres_text)
    $('#nombres_text').on('input', function() {
      var input=$(this);
      var is_name=input.val();
      if(is_name){input.removeClass("invalid").addClass("valid");}
      else{input.removeClass("valid").addClass("invalid");}
    });

    // Validación campo Apellidos (#apellidos_text)
    $('#apellidos_text').on('input', function() {
      var input=$(this);
      var is_name=input.val();
      if(is_name){input.removeClass("invalid").addClass("valid");}
      else{input.removeClass("valid").addClass("invalid");}
    });

    // Validación campo Email (#mail_email)
    $('#mail_email').on('input', function() {
    	var input=$(this);
    	var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    	var is_email=re.test(input.val());
    	if(is_email){input.removeClass("invalid").addClass("valid");}
    	else{input.removeClass("valid").addClass("invalid");}
    });

    // Validación campo Teléfono (#celular_number)
    $('#celular_number').on('input', function() {
      var input=$(this);
      var is_name=input.val();
      if(is_name){input.removeClass("invalid").addClass("valid");}
      else{input.removeClass("valid").addClass("invalid");}
    });




    // ANCHOR NAVIGATION

    $('#btn-anterior').on('click', function(event){     
      event.preventDefault();
      //$('html,body').animate({ scrollTop: $('#title-1').offset().top }, 'slow'); 
      $('html,body').animate({ scrollTop: $('.title-header').offset().top }, 'slow');       
    });    

    $('#next-step0').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('.title-header').offset().top }, 'slow');       
    });    
    
    $('#next-step1').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('#title-2').offset().top }, 'slow'); 

    });

    $('.btn-ajustar').on('click', function(event){     
        event.preventDefault(); 
        $('html,body').animate({ scrollTop: $('#title-2').offset().top }, 'slow'); 
        // Función para asignar valores AJUSTES -> Datos cargados desde el cotizador 
        // Valor cuota inicial debe calcularse
        var cuota_calc = 4500000;
        // Valor financiacion debe calcularse
        var financiar_calc = 315000000;
        asignarSepFinCotizador(cuota_calc, financiar_calc);
        currencyCheck();        
    });

    $('.btn-verpropuesta').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('#title-2').offset().top }, 'slow'); 
    });

    $('#btn-cotizar').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('#title-3').offset().top }, 'slow'); 
    });

  
});
