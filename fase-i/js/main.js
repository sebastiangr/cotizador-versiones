$(document).ready(function() {

    // AUTONUMERIC OPTIONS
    const autoNumericOptions = {
      currencySymbol: "$",
      decimalPlaces: 0
    };


    // TOOLTIPS
    $('[data-toggle="tooltip"]').tooltip()

    // TODO: Organizar comentarios, eliminar variables no usadas.

    // VARIABLES
    // Valor del inmueble traído desde la DB - valor fijo como ejemplo
    var valor_inmueble = 450000000;
    var inmueble_acabados = 1500000;
    var inmueble_primaaltura = 0;
    var inmueble_primavista = 0;
    var inmueble_total = valor_inmueble + inmueble_acabados + inmueble_primaaltura + inmueble_primavista;

    // Cuota inicial correspondiente al 30% del valor del inmueble
    var cuota_inicial = valor_inmueble * 0.3;
    //var cuota_inicial = 5000000;

    // Separación definida por constructora-inmobiliaria - 7% del valor_inmueble como ejemplo
    // var separacion = Math.round(valor_inmueble * 0.07);
    var separacion = 5000000;

    // Valor por financiar, valor_inmueble - valor
    var valor_faltante;

    // Valor faltante Cuota Inicial - se calcula al ingresar valor de separación: cuota_inicial - valor separación
    var valor_faltantecuotainicial;

    // Valor PROPUESTA porcentaje cuota inicial
    var porcentaje_cuotainicial = 30;

    // Valor PROPUESTA porcentaje separación
    var porcentaje_separacion = 12;

    // Valor PROPUESTA meses
    var propuesta_meses = 24;

    // Valor PROPUESTA cuota mensual
    var propuesta_cuotamensual = 5000000;

    // Valor PROPUESTA plazo
    var propuesta_plazo = 12;
    
    // Valor PROPUESTA interés
    var propuesta_interes = 1.1;    


    // Valor a FINANCIAR - CÁLCULO DE VALORES FINANCIEROS DESCUENTO O RECARGO
    var valor_financiar = 325000000;
    var financiacion_ingresos = 4500000;
    var financiacion_cuotamensual = 1250000;
    var financiacion_meses = 72;


    // VARIABLES INFORMACIÓN PROYECTO -> Datos traídos de la DB al seleccionar un proyecto
    var proyecto_nombre = "Nombre";
    var proyecto_etapa = 2;
    var proyecto_inmueble = "Apt 203";
    var proyecto_area = 180;
    var proyecto_parqueadero = "SI";
    var proyecto_kit = "Kit 1";
    var proyecto_valor = 450000000;


    // ASIGNACIÓN MANUAL INICIAL VALORES PROPUESTA -> Datos traídos desde DB
    // $("#propuesta_cuotainicial").text(cuota_inicial); // Cuota inicial
    // $("#propuesta_valor").text(valor_inmueble); // Valor inmueble
    // $("#propuesta_separación").text(separacion); // Separación
    // $("#propuesta_meses").text(propuesta_meses); // Meses saldo cuota inicial
    // $("#propuesta_valorcuota").text(propuesta_cuotamensual); // Valor cuota inicial

    // FUNCIÓN PARA ASIGNAR VALORES PROPUESTA CUOTA INICIAL
    function asignarPropuestaCuotaInicial(cuota_inicial, valor_inmueble, separacion, propuesta_meses, propuesta_cuotamensual) {
      $("#propuesta_cuotainicial").text(cuota_inicial); // Cuota inicial
      $("#propuesta_valor").text(valor_inmueble); // Valor inmueble
      $("#propuesta_separación").text(separacion); // Separación
      $("#propuesta_meses").text(propuesta_meses); // Meses saldo cuota inicial
      $("#propuesta_valorcuota").text(propuesta_cuotamensual); // Valor cuota inicial
      $("#total_cuotainicial").text(cuota_inicial); // Total cuota inicial / bloque ajustes     
    }
    // Llamado de la función:
    asignarPropuestaCuotaInicial(cuota_inicial, valor_inmueble, separacion, propuesta_meses, propuesta_cuotamensual);
    

    // ASIGNACIÓN MANUAL VALORES PROPUESTA FINANCIACIÓN -> Datos traídos desde DB
    // $("#financiacion_valor").text(valor_financiar); // Valor a financiar
    // $("#financiacion_ingresos").text(financiacion_ingresos); // Ingresos familiares requeridos
    // $("#financiacion_cuotamensual").text(financiacion_cuotamensual); // Cuota mensual estimada
    // $("#financiacion_meses").text(financiacion_meses); // Meses estimados en plan de pagos

    // FUNCIÓN PARA ASIGNAR VALORES PROPUESTA FINANCIACIÓN
    function asignarPropuestaFinanciacion(valor_financiar, financiacion_ingresos, financiacion_cuotamensual, financiacion_meses) {
      $("#financiacion_valor").text(valor_financiar); // Valor a financiar
      $("#financiacion_ingresos").text(financiacion_ingresos); // Ingresos familiares requeridos
      $("#financiacion_cuotamensual").text(financiacion_cuotamensual); // Cuota mensual estimada
      $("#financiacion_meses").text(financiacion_meses); // Meses estimados en plan de pagos
    }
    // Llamado de la función:
    asignarPropuestaFinanciacion(valor_financiar, financiacion_ingresos, financiacion_cuotamensual, financiacion_meses);
        

    // FUNCIÓN PARA ASIGNAR VALORES AJUSTES CUOTA INICIAL
    function asignarAjustesCuotaInicial(cuota_inicial, valor_inmueble, separacion, propuesta_meses, propuesta_cuotamensual) {
      $("#ajuste-propuesta_cuotainicial").text(cuota_inicial); // Cuota inicial
      $("#ajuste-propuesta_valor").text(valor_inmueble); // Valor inmueble
      $("#ajuste-propuesta_separación").text(separacion); // Separación
      $("#ajuste-propuesta_meses").text(propuesta_meses); // Meses saldo cuota inicial
      $("#ajuste-propuesta_valorcuota").text(propuesta_cuotamensual); // Valor cuota inicial
    }

    
    // FUNCIÓN PARA ASIGNAR VALORES AJUSTES PROPUESTA FINANCIACIÓN
    function asignarAjustesFinanciacion(valor_financiar, financiacion_ingresos, financiacion_cuotamensual, financiacion_meses) {
      $("#ajuste-financiacion_valor").text(valor_financiar); // Valor a financiar
      $("#ajuste-financiacion_ingresos").text(financiacion_ingresos); // Ingresos familiares requeridos
      $("#ajuste-financiacion_cuotamensual").text(financiacion_cuotamensual); // Cuota mensual estimada
      $("#ajuste-financiacion_meses").text(financiacion_meses); // Meses estimados en plan de pagos
    }


    // FUNCIÓN PARA ASIGNAR VALORES SIDEBAR - INFORMACIÓN PROYECTO
    function asignarInformacionProyecto(proyecto_nombre, proyecto_etapa, proyecto_inmueble, proyecto_area, proyecto_parqueadero, proyecto_kit, proyecto_valor) {
      $("#proyecto-nombre").text(proyecto_nombre); // Nombre del proyecto
      $("#proyecto-etapa").text(proyecto_etapa); // Etapa del proyecto
      $("#proyecto-inmueble").text(proyecto_inmueble); // Inmueble del proyecto
      $("#proyecto-area").text(proyecto_area); // Área del inmueble
      $("#proyecto-parqueadero").text(proyecto_parqueadero); // Parqueadero
      $("#proyecto-kit").text(proyecto_kit); // _Kit de acabados
      $("#proyecto-valor").text(proyecto_valor); // Valor inmueble
    }

    // FUNCIÓN PARA ASIGNAR VALORES SIDEBAR - RESUMEN COSTOS
    function asignarResumenCostos(resumen_precio, resumen_acabados, resumen_primaaltura, resumen_primavista, resumen_total) {
      $("#resumen-precio").text(resumen_precio); // Nombre del proyecto
      $("#resumen-acabados").text(resumen_acabados); // Etapa del proyecto
      $("#resumen-primaalatura").text(resumen_primaaltura); // Inmueble del proyecto
      $("#resumen-primavista").text(resumen_primavista); // Área del inmueble
      $("#resumen-total").text(resumen_total); // Parqueadero
    }    

    // FUNCIÓN PARA ASIGNAR VALORES SIDEBAR - SEPARACIÓN Y FINANCIACIÓN -> Valores predefinidos desde DB
    function asignarSepFinPropuesta(valor_separacion, valor_numcuotas, valor_valcuota, valor_financiar, valor_plazo, valor_interes) {
      $("#table-separacion").text(valor_separacion); // 
      $("#table-numcuotas").text(valor_numcuotas); // 
      $("#table-valcuota").text(valor_valcuota); // 
      $("#table-financiar").text(valor_financiar); // 
      $("#table-plazo").text(valor_plazo); // 
      $("#table-interes").text(valor_interes); // 
    }      
    
    // FUNCIÓN PARA ASIGNAR VALORES SIDEBAR - SEPARACIÓN Y FINANCIACIÓN -> Valores cotizador no modificables por el usuario
    function asignarSepFinCotizador(valor_cuota, valor_financiar) {
      $("#table-valcuota").text(valor_cuota); // Valor cuota mensual
      $("#table-financiar").text(valor_financiar); // Valor a financiar
    }           


    // CURRENCY CHECK
    function currencyCheck() {
      const currenCheck = AutoNumeric.multiple('.currency', {
        currencySymbol: "$",
        decimalPlaces: 0
      });
    }
      
    

    //Mostrar bloque Prima al seleccionar opción SI
    $("#prima_select").change(function() {
      if ($(this).val() == "SI") {
        $('#prima-block').slideDown();
      } else {
        $('#prima-block').slideUp();
      }
    });

    //Mostrar bloque Cesantías al seleccionar opción SI
    $("#cesantia_select").change(function() {
      if ($(this).val() == "SI") {
        $('#cesantia-block').slideDown();
      } else {
        $('#cesantia-block').slideUp();
      }
    });

    //Mostrar bloque Pagos Adicionales al seleccionar opción SI
    $("#adicionales_select").change(function() {
      if ($(this).val() == "SI") {
        $('#adicionales-block').slideDown();
      } else {
        $('#adicionales-block').slideUp();
      }
    });    
    


    // BLOQUES INICIALES SEPARACIÓN
    $("#resumen_separacion").text(separacion);
    $("#resumen_cuotainicial").text(cuota_inicial);
    $("#resumen_saldocuotainicial").text(0);
    $("#resumen_valorcuotamensual").text(0);

    //VALORES SIDEBAR - RESUMEN PLAN DE PAGOS
    $("#table-separacion").text(separacion);
    // $("#table-numcuotas").text(separacion);
    // $("#table-valcuota").text(separacion);


    // BOTÓN VOLVER A EMPEZAR - REINICIAR VALORES




    //Mostrar bloque Proyecto al seleccionar alguno
    $("#proyecto_select").change(function() {
      if ($(this).val() != "") {
        $('#form_proyecto').slideDown();
      } else {
        $('#form_proyecto').slideUp();
      }
    });

    //Mostrar bloque Inmuebles al seleccionar Etapa
    $("#etapa_select").change(function() {
      if ($(this).val() != "") {


        // if tipo A show, if tipo B show, if tipo C show
        if ($(this).val() == "Tipo A") {
          $('#listado_inmuebles_A').slideDown();
          $('#listado_inmuebles_B').slideUp();
          $('#listado_inmuebles_C').slideUp();
        } else if ($(this).val() == "Tipo B") {
          $('#listado_inmuebles_A').slideUp();
          $('#listado_inmuebles_B').slideDown();
          $('#listado_inmuebles_C').slideUp();
        } else if ($(this).val() == "Tipo C") {
          $('#listado_inmuebles_A').slideUp();
          $('#listado_inmuebles_B').slideUp();
          $('#listado_inmuebles_C').slideDown();
        }

      } else {
        $("#info-proyecto-block").collapse('hide');
        $("#info-resumen-block").collapse('show');
        $('#listado_inmuebles_A').slideUp();
        $('#listado_inmuebles_B').slideUp();
        $('#listado_inmuebles_C').slideUp();
      }
    });

    //Mostrar wrapper Subsidio al seleccionar VIS
    // $("#tipopproyecto_select").change(function() {
    //   if ($(this).val() == "vis") {
    //     $('.subsidio-row').addClass('show');
    //   } else {
    //     $('.subsidio-row').removeClass('show');
    //   }
    // });

    //Mostrar bloque Subsidio al seleccionar opción SI
    // $("#subsidio_select").change(function() {
    //   if ($(this).val() == "SI") {
    //     $('#subsidio-block').slideDown();
    //     $('#subsidio-tipo').show();
    //   } else {
    //     $('#subsidio-block').slideUp();
    //     $('#subsidio-tipo').hide();
    //   }
    // });

    // $("#subsidio_switch").click(function(){
    //   $('#subsidio-block').toggle();
    //   $('#subsidio-tipo').toggle();
    // });

    //Mostrar bloque Prima al seleccionar opción SI
    // $("#prima_select").change(function() {
    //   if ($(this).val() == "SI") {
    //     $('#prima-block').slideDown();
    //   } else {
    //     $('#prima-block').slideUp();
    //   }
    // });

    //Mostrar bloque Cesantías al seleccionar opción SI
    // $("#cesantia_select").change(function() {
    //   if ($(this).val() == "SI") {
    //     $('#cesantia-block').slideDown();
    //   } else {
    //     $('#cesantia-block').slideUp();
    //   }
    // });

    //Mostrar bloque Pagos Adicionales al seleccionar opción SI
    // $("#adicionales_select").change(function() {
    //   if ($(this).val() == "SI") {
    //     $('#adicionales-block').slideDown();
    //   } else {
    //     $('#adicionales-block').slideUp();
    //   }
    // });


    // $('.add_planpagos').on('click', function(e) {
    //   $('#plan-pagos').toggle();
    // });



    // SELECTORES PASO 1

    // Selector de inmuebles (inmueble-block)
    var inmueblesBlock = document.getElementsByClassName('inmueble-block');
    for (var i = 0; i < inmueblesBlock.length; i++) {
      inmueblesBlock[i].addEventListener("click", function(){

      var selectedBlock = document.querySelector(".selected");
      if(selectedBlock){
        selectedBlock.classList.remove("selected");
      }
        asignarInformacionProyecto(proyecto_nombre, proyecto_etapa, proyecto_inmueble, proyecto_area, proyecto_parqueadero, proyecto_kit, proyecto_valor); // Llamado función para asignar valores
        $("#info-proyecto-block").collapse('show'); // Mostrar bloque Proyecto en el sidebar
        asignarResumenCostos(valor_inmueble, inmueble_acabados, inmueble_primaaltura, inmueble_primavista, inmueble_total); // Llamado función para asignar valores 
        $("#info-resumen-block").collapse('show');  // Mostrar bloque Resumen en el sidebar
        $('#next-step1').prop('disabled', false); // Activar botón siguiente en paso 1 al seleccionar cualquier item
        this.classList.add("selected");
        currencyCheck();
      }, false);;
    }


    // Selector Parqueadero y Cuarto útil
    $("select#parqueadero_select").change(function(){
      var selectedParqueadero = $(this).children("option:selected").val();
      $("#parqueadero-valor").text(selectedParqueadero);
      anElement = new AutoNumeric("#parqueadero-valor", autoNumericOptions);
    });   
    $("select#cuartoutil_select").change(function(){
      var selectedCuartoutil = $(this).children("option:selected").val();
      $("#cuartoutil-valor").text(selectedCuartoutil);
      anElement = new AutoNumeric("#cuartoutil-valor", autoNumericOptions);
    });     


    // ACCIONES INPUT VALOR SEPARACIÓN
    const valorseparacion = new AutoNumeric('#valorSeparacion', {
      decimalPlaces: 0
    });  

    $("#valorSeparacion").keyup(function(e) {

      // Guardar el valor del input sin formato
      valueinput = valorseparacion.getNumber();
      // Valor máximo
      var max = parseInt($(this).attr('max'));
      // Valor minimo
      var min = parseInt($(this).attr('min'));        

      if (valueinput > max) {
        $('#valor-maximo').show();
        $('#valor-minimo').hide();
        $('#btn-financiar').prop('disabled', true);
        $('#btn-recalcular').prop('disabled', true);
      } else if (valueinput < min) {
        $('#valor-maximo').hide();
        $('#valor-minimo').show();
        $('#btn-financiar').prop('disabled', true);
        $('#btn-recalcular').prop('disabled', true);        
      } else {
        $('#valor-maximo').hide();
        $('#valor-minimo').hide();
        $('#btn-financiar').prop('disabled', false);
        //$('#btn-recalcular').prop('disabled', false);   
        $("#saldo_cuotainicial").text(cuota_inicial - valueinput); // Total cuota inicial - valor de separación
        currencyCheck();
      }        
    });


    // PRIMA CESANTIA SUBSIDIO ADICIONAL FIELDS
    const subsidiovalor = new AutoNumeric('.subsidio_valor', {
      decimalPlaces: 0
    });      
    const primavalor = new AutoNumeric('.prima_valor', {
      decimalPlaces: 0
    });  
    const cesantiavalor = new AutoNumeric('.cesantia_valor', {
      decimalPlaces: 0
    });  
    const adicionalvalor = new AutoNumeric('.adicional_valor', {
      decimalPlaces: 0
    });  
  
  
    //Mostrar bloque Subsidio al seleccionar opción SI
    $("#subsidio_select").change(function() {
      if ($(this).val() == "SI") {
        $('#subsidio-block').slideDown();
        $('#subsidio-tipo').show();
      } else {
        $('#subsidio-block').slideUp();
        $('#subsidio-tipo').hide();
      }
    });


    // SUBSIDIO ADD fields
    var maxFieldSubsidio = 2; //Input fields increment limitation
    var addSubsidio = $('.add_subsidio'); //Add button selector
    var wrapperSubsidio = $('.subsidio-wrapper'); //Input field wrapper

    var htmlSubsidio = '<div class="cuotas-group"><div class="cuotas-half"><div class="select-list"><select class="form-control" name="subsidio_tipo" id="subsidio_tipo" required><option disabled selected value> -- Selecciona una opción -- </option><option value="Gobierno Nacional">Gobierno Nacional</option><option value="Alcaldía Local">Alcaldía Local</option></select></div></div><div class="cuotas-half cuotas-half_second"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="" class="subsidio_valor form-control" placeholder="0" required></div></div><div class="cuotas-btn"><button class="remove_button"><i class="fa fa-minus"></i></button></div></div>'
    //var fieldHTML = '<div class="cuotas-group"><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button">-</a></div>'; //New input field html
    var x_subsidio = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addSubsidio).click(function(){
        //Check maximum number of input fields
        if(x_subsidio < maxFieldSubsidio){
            x_subsidio++; //Increment field counter
            $(wrapperSubsidio).append(htmlSubsidio); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapperSubsidio).on('click', '.remove_button', function(e){
        e.preventDefault();
        //$(this).parent('div').remove(); //Remove field html
        $(this).closest('.cuotas-group').remove(); //Remove field html
        x_subsidio--; //Decrement field counter
    });


    // PRIMA ADD fields
    var maxFieldPrima = 4; //Input fields increment limitation
    var addPrima = $('.add_prima'); //Add button selector
    var wrapperPrima = $('.prima-wrapper'); //Input field wrapper
    var htmlPrima = '<div class="cuotas-group"><div class="cuotas-half"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="" class="prima_valor form-control" placeholder="0" required></div></div><div class="cuotas-half cuotas-half_second"><div class="input-group input-date"><input type="date" name="prima_fecha" class="prima_fecha"><i class="fa fa-calendar"></i></div></div><div class="cuotas-btn"><button class="remove_button"><i class="fa fa-minus"></i></button></div></div>'
    //var fieldHTML = '<div class="cuotas-group"><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button">-</a></div>'; //New input field html
    var x_prima = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addPrima).click(function(){
        //Check maximum number of input fields
        if(x_prima < maxFieldPrima){
            x_prima++; //Increment field counter
            $(wrapperPrima).append(htmlPrima); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapperPrima).on('click', '.remove_button', function(e){
        e.preventDefault();
        //$(this).parent('div').remove(); //Remove field html
        $(this).closest('.cuotas-group').remove(); //Remove field html
        x_prima--; //Decrement field counter
    });


    // CESANTÍA ADD fields
    var maxFieldCesantia = 4; //Input fields increment limitation
    var addCesantia = $('.add_cesantia'); //Add button selector
    var wrapperCesantia = $('.cesantia-wrapper'); //Input field wrapper
    var htmlCesantia = '<div class="cuotas-group"><div class="cuotas-half"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="" class="cesantia_valor form-control" placeholder="0" required></div></div><div class="cuotas-half cuotas-half_second"><div class="input-group input-date"><input type="date" name="cesantia_fecha" class="cesantia_fecha"><i class="fa fa-calendar"></i></div></div><div class="cuotas-btn"><button class="remove_button"><i class="fa fa-minus"></i></button></div></div>'
    var x_cesantia = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addCesantia).click(function(){
        //Check maximum number of input fields
        if(x_cesantia < maxFieldCesantia){
            x_cesantia++; //Increment field counter
            $(wrapperCesantia).append(htmlCesantia); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapperCesantia).on('click', '.remove_button', function(e){
        e.preventDefault();
        //$(this).parent('div').remove(); //Remove field html
        $(this).closest('.cuotas-group').remove(); //Remove field html
        x_cesantia--; //Decrement field counter
    });


    // ADICIONAL ADD fields
    var maxFieldAdicional = 5; //Input fields increment limitation
    var addAdicional = $('.add_adicional'); //Add button selector
    var wrapperAdicional = $('.adicional-wrapper'); //Input field wrapper
    var htmlAdicional = '<div class="cuotas-group"><div class="cuotas-half"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="" class="adicional_valor form-control" placeholder="0" required></div></div><div class="cuotas-half cuotas-half_second"><div class="input-group input-date"><input type="date" name="adicional_fecha" class="adicional_fecha"><i class="fa fa-calendar"></i></div></div><div class="cuotas-btn"><button class="remove_button"><i class="fa fa-minus"></i></button></div></div>'
    var x_adicional = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addAdicional).click(function(){
        //Check maximum number of input fields
        if(x_adicional < maxFieldAdicional){
            x_adicional++; //Increment field counter
            $(wrapperAdicional).append(htmlAdicional); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapperAdicional).on('click', '.remove_button', function(e){
        e.preventDefault();
        //$(this).parent('div').remove(); //Remove field html
        $(this).closest('.cuotas-group').remove(); //Remove field html
        x_adicional--; //Decrement field counter
    });


    // RANGE SLIDER - VALOR
    var $rangeslider = $('#slider-range');
    var $valor_cuotamensual = $('#valor_cuotamensual');

    var $valor_cuotamensual_table = $('#table-valcuota');
    var $num_cuotas_table = $('#table-numcuotas');
    var $valor_financiar_table = $('#table-financiar');
    var $plazo_table = $('#table-plazo');
    var $interes_table = $('#table-interes');

    var $resumen_saldocuotainicial = $("#resumen_saldocuotainicial");
    var $resumen_valorcuotamensual = $("#resumen_valorcuotamensual");


    // RANGE SLIDER - CUOTA
    var $rangeslidercuota = $('#slider-range-cuota');
    const $elementcuota = $('#slider-range-cuota');
    var $handlecuota;

    $elementcuota
      .rangeslider({
        polyfill: false,
        rangeClass: 'rangeslider rangeslider--plazo',

        onInit: function() {
          $handlecuota = $('.rangeslider__handle', this.$range);
          updateHandle($handlecuota[0], this.value);
        }
      })
      .on('input', function() {
        updateHandle($handlecuota[0], this.value);
        // CÁLCULO DE CUOTA MENSUAL -> se requiere fórmula
        $valor_cuotamensual.text(Math.round((cuota_inicial - $rangeslider.val())/this.value));

        valor_faltantecuotainicial = cuota_inicial - $rangeslider.val();

        // BLOQUES INICIALES
        if (valor_faltantecuotainicial >= 0) {
          $resumen_saldocuotainicial.text(valor_faltantecuotainicial);
          $resumen_valorcuotamensual.text(Math.round((cuota_inicial - $rangeslider.val())/$rangeslidercuota.val()));
        } else {
          $resumen_saldocuotainicial.text(0);
          $resumen_valorcuotamensual.text(0);
        }

        // SIDEBAR - RESUMEN PLAN DE PAGOS VALUES
        $valor_cuotamensual_table.text(Math.round((cuota_inicial - $rangeslider.val())/this.value));
        $num_cuotas_table.text(this.value);

      });

    // Update the value inside the slider handle
    function updateHandle(el, val) {
      el.textContent = val;
    }


    // RANGE SLIDER - PLAZO
    var $rangesliderplazo = $('#slider-range-plazo');
    const $elementplazo = $('#slider-range-plazo');
    const $tooltip = $('#slider-range-plazo');
    var $handle;

    $elementplazo
      .rangeslider({
        polyfill: false,
        rangeClass: 'rangeslider rangeslider--plazo',

        onInit: function() {
          $handle = $('.rangeslider__handle', this.$range);
          updateHandle($handle[0], this.value);
        }
      })
      .on('input', function() {
        $('#resumen-meses').text(this.value*12);
        updateHandle($handle[0], this.value);

        $plazo_table.text(this.value);
      });

    // Update the value inside the slider handle
    function updateHandle(el, val) {
      el.textContent = val;
    }



    // DEFINE FIRST VALUES
    // valor_faltante = valor_inmueble - $rangeslider.value;
    $valor_financiar_table.text(valor_inmueble -  $rangeslider.val());
    $plazo_table.text($elementplazo.val());
    var tasav =$("#tasainteres_number").val();
    $interes_table.text(tasav);


    $("#tasainteres_number").keyup(function(){
      $interes_table.text(this.value);
    });



    // NAVEGACIÓN //

    // Botón CALCULAR
    $('#btn-financiar').on('click', function(e) {
      // AÑADIR LOADING
      // $('i.loading').addClass('icon-visible');
      $('#btn-financiar i').addClass('i-visible');
      setTimeout(function(){
        // FUNCIÓN AJUSTAR CUOTA INICIAL - Cargar valores nuevos desde servidor / DB
        asignarAjustesCuotaInicial(cuota_inicial, valor_inmueble, separacion, propuesta_meses, propuesta_cuotamensual);      
        $('#row-ajuste-cuotainicial').toggleClass( "visible" );      
        // FUNCIÓN AJUSTAR FINANCIACIÓN - Cargar valores nuevos desde servidor / DB
        asignarAjustesFinanciacion(valor_financiar, financiacion_ingresos, financiacion_cuotamensual, financiacion_meses);      
        $('#row-ajuste-financiacion').toggleClass( "visible" );
        $('#btn-financiar').hide(); 
        $('#btn-recalcular').show();
        currencyCheck();
      }, 4000); 
    });

    // Botón RECALCULAR
    $('#btn-recalcular').on('click', function(e) {
      // AÑADIR LOADING
      // $('i.loading').addClass('icon-visible');
      $('#btn-recalcular i').addClass('i-visible');
      setTimeout(function(){
        // FUNCIÓN AJUSTAR CUOTA INICIAL - Cargar valores nuevos desde servidor / DB
        asignarAjustesCuotaInicial(cuota_inicial, valor_inmueble, separacion, propuesta_meses, propuesta_cuotamensual);         
        // FUNCIÓN AJUSTAR FINANCIACIÓN - Cargar valores nuevos desde servidor / DB
        asignarAjustesFinanciacion(valor_financiar, financiacion_ingresos, financiacion_cuotamensual, financiacion_meses);      

        $('#btn-recalcular i').removeClass('i-visible');
        currencyCheck();
      }, 4000); 
    });

    // Botón AJUSTAR FORMAS DE PAGO
    $('.btn-ajustar').on('click', function(e) {
      $('#cuotas-container').slideToggle();
      $('#plan-pagos').slideToggle();
      $('#financiacion-container').slideToggle();
      $('#propuestacuotainicial-block').slideToggle();
      $('#propuestafinanciacion-block').slideToggle();  
      $('.btn-verpropuesta').show();
      $('.btn-ajustar').hide();
      $('span.text-propuesta').removeClass('show');
      // $(this).hide();
    });

    // Botón VER PROPUESTA
    $('.btn-verpropuesta').on('click', function(e) {
      $('#cuotas-container').slideToggle();
      $('#plan-pagos').slideToggle();
      $('#financiacion-container').slideToggle();
      $('#propuestacuotainicial-block').slideToggle();
      $('#propuestafinanciacion-block').slideToggle();  
      $('.btn-verpropuesta').hide();
      $('.btn-ajustar').show();
      $('span.text-propuesta').addClass('show');  
      // $(this).hide();
    });

    // Botón VOLVER A EMPEZAR
    $('.btn-reiniciar').on('click', function(e) {
      $("#proyecto_select")[0].selectedIndex = 0;
      $("#tipopproyecto_select")[0].selectedIndex = 0;
      $("#etapa_select")[0].selectedIndex = 0;
      $("#parqueadero_select")[0].selectedIndex = 0;
      $("#cuartoutil_select")[0].selectedIndex = 0;
      $("#otro_select")[0].selectedIndex = 0;   
      $("#form_proyecto").slideUp();
      $(".listado_inmuebles").slideUp();
      $('#next-step1').prop('disabled', true); 
      $(".inmueble-block").removeClass("selected");
      asignarInformacionProyecto("", "", "", "", "", "", "");
      asignarResumenCostos("","","","","");
      $("#info-proyecto-block").collapse('hide'); // Esconder bloque Proyecto en el sidebar
      $("#info-resumen-block").collapse('hide');  // Esconder bloque Resumen en el sidebar

    });    



    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');

        if (!$item.hasClass('disabled')) {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show();
        }
    });

    $('ul.setup-panel li.active a').trigger('click');

    // STEP 0 //
    $('.activate-step-0').on('click', function(e) {
      $('ul.setup-panel li:eq(0)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-0"]').trigger('click');
      // $('.menu-lateral-step1').show();
      // $('.menu-lateral-step2').hide();
      // $('.menu-lateral-step3').hide();
    });
    // STEP 1 //
    $('.activate-step-1').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-1"]').trigger('click');
        $('.menu-lateral-step1').show();
        $('.menu-lateral-step2').hide();
        $('.menu-lateral-step3').hide();
    });
    // STEP 2 //
    $('.activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
        $('.menu-lateral-step1').hide();
        $('.menu-lateral-step2').show();
        $('.menu-lateral-step3').hide();
        //$(this).remove();
    });
    // STEP 3 //
    $('.activate-step-3').on('click', function(e) {
      $('ul.setup-panel li:eq(3)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
      $('.menu-lateral-step1').hide();
      $('.menu-lateral-step2').hide();
      $('.menu-lateral-step3').show();
    });
    // STEP 4 //
    $('.activate-step-4').on('click', function(e) {
      $('ul.setup-panel li:eq(4)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-4"]').trigger('click');
      // $('.menu-lateral-step1').hide();
      // $('.menu-lateral-step2').hide();
      // $('.menu-lateral-step3').show();
    });   

    // BTN COTIZAR - IGUAL A SIGUIENTE EN STEP 3
    $('#btn-cotizar').on('click', function(e) {
      $('ul.setup-panel li:eq(2)').removeClass('disabled');
      $('ul.setup-panel li a[href="#step-3"]').trigger('click');
      $('.menu-lateral-step1').hide();
      $('.menu-lateral-step2').hide();
      $('.menu-lateral-step3').show();
    });        


    // FORM VALIDATION - FALTAN
    // Validación campo Nombre (#nombres_text)
    $('#nombres_text').on('input', function() {
      var input=$(this);
      var is_name=input.val();
      if(is_name){input.removeClass("invalid").addClass("valid");}
      else{input.removeClass("valid").addClass("invalid");}
    });

    // Validación campo Apellidos (#apellidos_text)
    $('#apellidos_text').on('input', function() {
      var input=$(this);
      var is_name=input.val();
      if(is_name){input.removeClass("invalid").addClass("valid");}
      else{input.removeClass("valid").addClass("invalid");}
    });

    // Validación campo Email (#mail_email)
    $('#mail_email').on('input', function() {
    	var input=$(this);
    	var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    	var is_email=re.test(input.val());
    	if(is_email){input.removeClass("invalid").addClass("valid");}
    	else{input.removeClass("valid").addClass("invalid");}
    });

    // Validación campo Teléfono (#celular_number)
    $('#celular_number').on('input', function() {
      var input=$(this);
      var is_name=input.val();
      if(is_name){input.removeClass("invalid").addClass("valid");}
      else{input.removeClass("valid").addClass("invalid");}
    });




    // ANCHOR NAVIGATION

    $('#btn-anterior').on('click', function(event){     
      event.preventDefault();
      //$('html,body').animate({ scrollTop: $('#title-1').offset().top }, 'slow'); 
      $('html,body').animate({ scrollTop: $('.title-header').offset().top }, 'slow');       
    });    

    $('#next-step0').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('.title-header').offset().top }, 'slow');       
    });    
    
    $('#next-step1').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('#title-2').offset().top }, 'slow'); 
      // Función para asignar valores PROPUESTA -> Datos cargados desde DB
      asignarSepFinPropuesta(separacion, propuesta_meses, propuesta_cuotamensual, valor_financiar, propuesta_plazo, propuesta_interes);
      currencyCheck();
    });

    $('.btn-ajustar').on('click', function(event){     
        event.preventDefault(); 
        $('html,body').animate({ scrollTop: $('#title-2').offset().top }, 'slow'); 
        // Función para asignar valores AJUSTES -> Datos cargados desde el cotizador 
        // Valor cuota inicial debe calcularse
        var cuota_calc = 4500000;
        // Valor financiacion debe calcularse
        var financiar_calc = 315000000;
        asignarSepFinCotizador(cuota_calc, financiar_calc);
        currencyCheck();        
    });

    $('.btn-verpropuesta').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('#title-2').offset().top }, 'slow'); 
    });

    $('#btn-cotizar').on('click', function(event){     
      event.preventDefault();
      $('html,body').animate({ scrollTop: $('#title-3').offset().top }, 'slow'); 
    });

  
});
